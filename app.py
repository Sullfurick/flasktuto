from flask import Flask, request

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


db = ["clavier", "souris", "casque", "camion", "briquet"]


@app.route("/read", methods=["GET"])
def read_list():
    to_read_list = db
    print(to_read_list)
    return db


@app.route("/add", methods=["POST"])
def add_to_list():
    db.append(request.get_data())
    print(db)
    return "Nice !"
